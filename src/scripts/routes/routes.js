import Detail from '../views/pages/detail'
import Like from '../views/pages/like'
import ListRestaurant from '../views/pages/list-restaurant'

const routes = {
  '/': ListRestaurant, // default page,
  '/detail/:id': Detail,
  '/like': Like
}

export default routes
