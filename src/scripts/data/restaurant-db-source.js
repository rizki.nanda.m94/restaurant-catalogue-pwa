import API_ENDPOINT from '../globals/api-endpoint'

class RestauranDbSource {
  static async listRestaurants () {
    const response = await fetch(API_ENDPOINT.LIST_RESTAURANT)
    const responseJson = await response.json()
    return responseJson.restaurants
  }

  static async detailRestaurant (id) {
    const response = await fetch(API_ENDPOINT.DETAIL(id))

    const responseJson = await response.json()
    return responseJson.restaurant
  }

  static async addReview (id, name, review) {
    const urlencoded = new URLSearchParams()

    urlencoded.append('id', id)
    urlencoded.append('name', name)
    urlencoded.append('review', review)

    const response = await fetch(API_ENDPOINT.ADD_REVIEW, {
      method: 'POST',
      headers: {
        'X-Auth-Token': '12345',
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: urlencoded,
      redirect: 'follow'
    })

    const responseJson = await response.json()
    return responseJson.customerReviews
  }
}

export default RestauranDbSource
