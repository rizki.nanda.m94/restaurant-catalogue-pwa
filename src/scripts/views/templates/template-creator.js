import CONFIG from '../../globals/config'

const createRestaurantDetailTemplate = (restaurant) => `
  <h2 tabindex="0" class="restaurant__title">${restaurant.name}</h2>
  <img tabindex="0" class="restaurant__poster" src="${CONFIG.BASE_IMAGE_URL_MEDIUM + restaurant.pictureId}" alt="${restaurant.name}" />
  <div tabindex="0" class="restaurant__info">
    <h3>Information : </h3>
      <h4>Address</h4>
      <p>${restaurant.address}</p>
      <h4>City</h4>
      <p>${restaurant.city} minutes</p>
      <h4>Rating</h4>
      <p>${restaurant.rating}</p>
      <h4>Category</h4>
      <div id="category"></div>
  </div>
   <div tabindex="0" style="overflow-x:auto;">
    <table id="table-menu">
      <tr>
        <th>Menu makanan</th>
        <th>Menu Minuman</th>
      </tr>
    </table>
  </div>
  <div tabindex="0" class="restaurant__overview">
    <h3>Overview</h3>
    <p>${restaurant.description}</p>
  </div>
  <div tabindex="0" class="restaurant__review">
    <h3>Customer Review</h3>
  <div class="review-table-style">
    <table id="main-table">
      <tr>
        <th>Nama</th>
        <th>Review</th>
        <th>Date</th>
      </tr>
    </table>
  </div>
  </div>
  <br>
  <div tabindex="0" class="container-rev">
  <form action="">
    <div class="row">
      <div class="col-25">
      <label for="lname">Name</label>
      </div>
      <div class="col-75">
      <input type="text" name="fname" id="fname" placeholder="Your name..">
      </div>
    </div>
    <div class="row">
      <div class="col-25">
      <label for="subject">Review</label>
      </div>
      <div class="col-75">
      <textarea name="freview" id="freview" placeholder="Write something..." style="height:200px"></textarea>
      </div>
    </div>
    <div class="row">
      <input type="submit" id="submit_review" value="Submit">
    </div>
  </form>
  </div>
  `

const createRestaurantTemplate = (restaurant) => `
     <article tabindex="0" class="resto-item">
        <img class="resto-item__thumbnail lazyload"
                data-src="${restaurant.pictureId
? CONFIG.BASE_IMAGE_URL_SMALL + restaurant
    .pictureId
: 'https://picsum.photos/id/666/800/450?grayscale'}"
                alt="Gambar 0">
        <div class="resto-item__content">
            <p class="resto-item__city"> ${restaurant.city} - ⭐️ : ${restaurant.rating} </p> <br>
            <div class="resto-item__title"><a href="${`/#/detail/${restaurant.id}`}">${restaurant.name}</a></div>
            <p class="resto-item__description"> ${restaurant.description} </p>
        </div>
    </article>
`

const createLikeRestaurantButtonTemplate = () => `
  <button aria-label="like this restaurant" id="likeButton" class="like">
     <i class="fa fa-heart-o" aria-hidden="true"></i>
  </button>
`

const createUnlikeRestaurantButtonTemplate = () => `
  <button aria-label="unlike this restaurant" id="likeButton" class="like">
    <i class="fa fa-heart" aria-hidden="true"></i>
  </button>
`

export {
  createRestaurantTemplate,
  createRestaurantDetailTemplate,
  createLikeRestaurantButtonTemplate,
  createUnlikeRestaurantButtonTemplate
}
