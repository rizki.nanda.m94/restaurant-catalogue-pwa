import RestauranDbSource from '../../data/restaurant-db-source'
import UrlParser from '../../routes/url-parser'
import LikeButtonPresenter from '../../utils/like-button-presenter'
import { createRestaurantDetailTemplate } from '../templates/template-creator'

const Detail = {
  async render () {
    return `
            <h1 tabindex="0" class="latest__label">Detail Restaurant</h1>
            <div class="offline_msg"></div>
            <div class="restaurant" id="restaurant"></div>
            <div id="likeButtonContainer"></div>
        `
  },

  async afterRender () {
    const url = UrlParser.parseActiveUrlWithoutCombiner()
    const msgOffline = document.querySelector('.offline_msg')
    const heroElement = document.querySelector('.hero')
    heroElement.style.display = 'none'

    msgOffline.classList.add('style-offline')
    msgOffline.innerHTML = '<p>Anda sedang offline ...</p>'
    const restaurant = await RestauranDbSource.detailRestaurant(url.id)

    if (restaurant) {
      msgOffline.innerHTML = ''
      msgOffline.classList.add('style-offline')
    }

    const restaurantContainer = document.querySelector('#restaurant')
    restaurantContainer.innerHTML = createRestaurantDetailTemplate(restaurant)
    const category = document.querySelector('#category')
    const categoryOptions = restaurant.categories

    categoryOptions.forEach(item => {
      category.innerHTML += `<li>${item.name}</li>`
    })

    const menuTable = document.querySelector('#table-menu')
    const makanan = restaurant.menus.foods
    const minuman = restaurant.menus.drinks

    const jmMakanan = makanan.length

    makanan.slice().reverse()
    minuman.slice().reverse().forEach((mnm, index) => {
      const row = menuTable.insertRow(1)
      const cell1 = row.insertCell(0)
      const cell2 = row.insertCell(1)
      cell2.innerHTML = mnm.name

      if (index < jmMakanan) {
        cell1.innerHTML = makanan[index].name
      }
    })

    const mainTable = document.querySelector('#main-table')
    const reviews = restaurant.customerReviews

    reviews.slice().reverse().forEach((review) => {
      const row = mainTable.insertRow(1)
      const cell1 = row.insertCell(0)
      const cell2 = row.insertCell(1)
      const cell3 = row.insertCell(2)

      cell1.innerHTML = review.name
      cell2.innerHTML = review.review
      cell3.innerHTML = review.date
    })

    LikeButtonPresenter.init({
      likeButtonContainer: document.querySelector('#likeButtonContainer'),
      restaurant: {
        id: restaurant.id,
        name: restaurant.name,
        city: restaurant.city,
        description: restaurant.description,
        pictureId: restaurant.pictureId,
        rating: restaurant.rating
      }
    })

    const reviewBtn = document.querySelector('#submit_review')
    reviewBtn.addEventListener('click', function () {
      const name = document.querySelector('#fname').value
      const comment = document.querySelector('#freview').value

      RestauranDbSource.addReview(url.id, name, comment)
    })
  }

}

export default Detail
