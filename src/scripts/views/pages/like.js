import FavoriteRestaurantIdb from '../../data/favorite-restaurant-idb'
import { createRestaurantTemplate } from '../templates/template-creator'

const Like = {
  async render () {
    return `
            <section class="content">
                <div class="latest">
                    <h1 tabindex="0" class="latest__label">Favorite Restaurant</h1>
                    <div class="restaurants" id="restaurant">
                    </div>
                </div>
            </section>
            `
  },

  async afterRender () {
    const heroElement = document.querySelector('.hero')
    heroElement.style.display = 'none'
    const restaurants = await FavoriteRestaurantIdb.getAllRestaurantRestaurant()
    const restaurantContainer = document.querySelector('#restaurant')
    if (restaurants.length === 0) {
      restaurantContainer.innerHTML = '<div class="resto-item__not__found"><center>Tidak ada Restaurant untuk ditampilkan</center></div>'
    }
    restaurants.forEach(restaurant => {
      restaurantContainer.innerHTML += createRestaurantTemplate(restaurant)
    })
  }
}

export default Like
