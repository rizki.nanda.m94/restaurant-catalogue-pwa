import RestauranDbSource from '../../data/restaurant-db-source'
import { createRestaurantTemplate } from '../templates/template-creator'

const ListRestaurant = {
  async render () {
    return `
            <section class="content">
                <div class="latest">
                    <h1 tabindex="0" class="latest__label">Daftar Restaurant</h1>
                    <div class="loader" id="loader"></div>
                    <div class="restaurants" id="restaurant">
                    </div>
                </div>
            </section>    
            `
  },

  async afterRender () {
    const restaurants = await RestauranDbSource.listRestaurants()
    const restaurantContainer = document.querySelector('#restaurant')
    const heroElement = document.querySelector('.hero')
    heroElement.style.display = 'flex'
    const loaderElement = document.querySelector('.loader')

    restaurants.forEach((restaurant) => {
      restaurantContainer.innerHTML += createRestaurantTemplate(restaurant)

      loaderElement.classList.add('hide-loader')
    })
  }
}

export default ListRestaurant
