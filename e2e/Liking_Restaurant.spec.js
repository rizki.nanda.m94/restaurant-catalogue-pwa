const assert = require('assert')

Feature('Liking Restaurant')

Before(({ I }) => {
  I.amOnPage('/#/like')
})

Scenario('liking one restaurant', async ({ I }) => {
  I.see('Tidak ada Restaurant untuk ditampilkan', '.resto-item__not__found')

  I.amOnPage('/')

  I.seeElement('.resto-item__title a')
  const firstRestaurant = locate('.resto-item__title a').first()
  const firstRestaurantTitle = await I.grabTextFrom(firstRestaurant)

  I.click(firstRestaurant)
  I.seeElement('#likeButton')
  I.click('#likeButton')
  I.amOnPage('/#/like')
  I.seeElement('.resto-item')

  const likedRestaurantTitle = await I.grabTextFrom('.resto-item__title')
  assert.strictEqual(firstRestaurantTitle, likedRestaurantTitle)
})

Scenario('Unliking one restaurant', async ({ I }) => {
  I.see('Tidak ada Restaurant untuk ditampilkan', '.resto-item__not__found')

  I.amOnPage('/')

  I.seeElement('.resto-item__title a')
  const firstRestaurant = locate('.resto-item__title a').first()
  const firstRestaurantTitle = await I.grabTextFrom(firstRestaurant)
  I.click(firstRestaurant)
  I.seeElement('#likeButton')
  I.click('#likeButton')
  I.amOnPage('/#/like')
  I.seeElement('.resto-item')
  const likedRestaurantTitle = await I.grabTextFrom('.resto-item__title')
  assert.strictEqual(firstRestaurantTitle, likedRestaurantTitle)

  I.seeElement('.resto-item__title a')
  const firstFavRestaurant = locate('.resto-item__title a').first()
  I.click(firstFavRestaurant)
  I.seeElement('#likeButton')
  I.click('#likeButton')

  I.amOnPage('/#/like')

  I.see('Tidak ada Restaurant untuk ditampilkan', '.resto-item__not__found')
})

Scenario('Add review one restaurant', async ({ I }) => {
  I.see('Tidak ada Restaurant untuk ditampilkan', '.resto-item__not__found')

  I.amOnPage('/')

  I.seeElement('.resto-item__title a')
  const firstRestaurant = locate('.resto-item__title a').first()

  I.click(firstRestaurant)
  I.seeElement('#fname')
  I.seeElement('#freview')

  I.fillField('#fname', 'superboy')
  I.fillField('#freview', 'this is a review with e2e testing :)')

  I.seeElement('#submit_review')
  I.click('#submit_review')
})
